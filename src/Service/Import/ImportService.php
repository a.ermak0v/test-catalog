<?php

namespace App\Service\Import;

use App\Component\Import\XMLImporter;
use App\Entity\Import;

class ImportService
{
    private XMLImporter $importer;

    public function __construct(XMLImporter $importer)
    {
        $this->importer = $importer;
    }

    public function start(Import $import): Import
    {
        $this->importer->process($import);

        return $import;
    }
}
