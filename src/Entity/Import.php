<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use App\Component\Import\ImportStatus;
use App\Controller\ImportStartController;
use App\Repository\ImportRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ImportRepository::class)]
#[ApiResource(operations: [
    new Get(
        name: 'process',
        uriTemplate: '/imports/{id}/start',
        controller: ImportStartController::class
    )
])]
class Import
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $entity = null;

    #[ORM\Column(length: 10, enumType: ImportStatus::class)]
    private ImportStatus $status;

    #[ORM\Column(length: 255)]
    private ?string $input = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $output = null;

    public function __construct()
    {
        $this->status = ImportStatus::DRAFT;
    }

    public function getEntity(): ?string
    {
        return $this->entity;
    }

    public function setEntity(string $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ImportStatus
    {
        return $this->status;
    }

    public function setStatus(ImportStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getInput(): ?string
    {
        return $this->input;
    }

    public function setInput(string $input): self
    {
        $this->input = $input;

        return $this;
    }

    public function getOutput(): ?string
    {
        return $this->output;
    }

    public function setOutput(?string $output): self
    {
        $this->output = $output;

        return $this;
    }
}
