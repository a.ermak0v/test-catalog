<?php
namespace App\Component\Import;

enum ImportStatus: string
{
    case DRAFT = 'DRAFT';
    case PROCESS = 'PROCESS';
    case IMPORTED = 'IMPORTED';
}
