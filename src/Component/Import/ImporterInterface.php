<?php

namespace App\Component\Import;

use App\Entity\Import;

interface ImporterInterface
{
    public function process(Import $import): bool;
}
