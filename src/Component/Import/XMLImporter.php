<?php

namespace App\Component\Import;

use App\Entity\Import;
use App\Repository\ImportRepository;
use Doctrine\Persistence\ManagerRegistry;
use XMLReader;

class XMLImporter implements ImporterInterface
{
    private ImportRepository $importRepository;

    private ManagerRegistry $registry;

    public function __construct(ImportRepository $importRepository, ManagerRegistry $registry)
    {
        $this->registry = $registry;
        $this->importRepository = $importRepository;
    }

    private function makeXMLReader(string $file): XMLReader
    {
        $reader = new XMLReader();
        $reader->open($file);

        return $reader;
    }

    public function process(Import $import): bool
    {
        $reader = $this->makeXMLReader($import->getInput());
        $manager = $this->registry->getManagerForClass($import->getEntity());

        dd($manager->getMetadataFactory());

        while($reader->read()) {
            dump($reader);
        }

        return true;
    }
}
