<?php

namespace App\Controller;

use App\Entity\Import;
use App\Service\Import\ImportService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class ImportStartController extends AbstractController
{
    private ImportService $importService;

    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }

    public function __invoke(Import $import): Import
    {
        return $this->importService->start($import);
    }
}
