import Vue from 'vue';
import Vuex from 'vuex';

import App from './modules/app'
import Catalog from './modules/catalog'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        app: App,
        catalog: Catalog
    },
});
