import notification from "./notification";

export default {
    namespaced: true,
    modules: {
        notification
    }
};
