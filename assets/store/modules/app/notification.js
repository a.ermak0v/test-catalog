import {
    PUSH_NOTIFICATION,
    SHIFT_NOTIFICATIONS,
    CLEAR_NOTIFICATION
} from './mutations';

export default {
    state: {
        active: false,
        items: [],
        notification: {
            type: '',
            text: ''
        }
    },
    mutations: {
        [PUSH_NOTIFICATION](state, item) {
            state.items.push(item);
        },
        [SHIFT_NOTIFICATIONS](state) {
            state.notification = state.items.shift();
            state.active = true;
        },
        [CLEAR_NOTIFICATION](state) {
            state.active = false;
            state.notification = { type: '', text: '' };
        }
    },
    actions: {
        pushNotification({ state, commit }, notification) {
            commit(PUSH_NOTIFICATION, notification);

            if (!state.active) {
                commit(SHIFT_NOTIFICATIONS);
            }
        },
        clearNotification({ state, commit }) {
            commit(CLEAR_NOTIFICATION);

            if (state.items.length) {
                setTimeout(() => commit(SHIFT_NOTIFICATIONS), 300);
            }
        }
    }
};
