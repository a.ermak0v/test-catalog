import Product from "./product";

export default {
    namespaced: true,
    modules: {
        Product
    }
};
