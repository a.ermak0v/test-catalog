import serviceApi from "../../../api"
import {
    SET_PRODUCTS,
    SET_PRODUCTS_TOTAL,
    SET_PRODUCTS_FILTER,
    SET_PRODUCTS_STATUS
} from "./mutations";
import { LOADED, LOADING } from "../../statuses";

export default {
    state: {
        items: [],
        totalItems: 0,
        filter: {},
        status: LOADING
    },
    mutations: {
        [SET_PRODUCTS](state, data) {
            state.items = data;
        },
        [SET_PRODUCTS_TOTAL](state, totalItems) {
            state.totalItems = totalItems;
        },
        [SET_PRODUCTS_FILTER](state, filter) {
            state.filter = filter;
        },
        [SET_PRODUCTS_STATUS](state, status) {
            state.status = status;
        }
    },
    actions: {
        setProductsFilter({ commit, dispatch }, filter) {
            commit(SET_PRODUCTS_FILTER, filter);
        },

        async loadProducts({ commit, state, dispatch }) {
            commit(SET_PRODUCTS_STATUS, LOADING);

            try {
                const { data, meta } = await serviceApi.product.getAll(state.filter);

                commit(SET_PRODUCTS, data);
                commit(SET_PRODUCTS_TOTAL, meta.totalItems);
            } catch (error) {
                dispatch('app/pushNotification', {
                    type: 'error',
                    text: error.message
                }, { root: true });
            }

            commit(SET_PRODUCTS_STATUS, LOADED);
        },
    }
};
