import Vue from 'vue';
import Router from 'vue-router';
import DefaultLayout from "../layouts/DefaultLayout";
import ProductsView from "../views/ProductsView";
import ImportView from "../views/ImportView";
import CoverageView from "../views/CoverageView";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: DefaultLayout,
            redirect: {
                name: 'products',
            },
            children: [
                {
                    path: 'products',
                    name: 'products',
                    component: ProductsView,
                    meta: {
                        title: 'Продукты',
                    }
                },
                {
                    path: 'import',
                    name: 'import',
                    component: ImportView,
                    meta: {
                        title: 'Импорт',
                    }
                },
                {
                    path: 'coverage',
                    name: 'coverage',
                    component: CoverageView,
                    meta: {
                        title: 'Покрытие',
                    }
                },
            ]
        },
    ],
});
