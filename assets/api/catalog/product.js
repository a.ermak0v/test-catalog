export default class Product {
    constructor(api, url) {
        this._api = api;
        this._path = `${url}/api/products`;
    }

    getAll(params) {
        return this._api.get(this._path, params);
    }
}
