import Api from "./api";
import Product from "./catalog/product";

class ServiceApi {
    constructor(api, url) {
        this._api = api;
        this._url = url;
        this.product = new Product(this._api, this._url);
    }
}

const api = new Api();
const serviceApi = new ServiceApi(api, 'http://localhost:8000');

export default serviceApi;
