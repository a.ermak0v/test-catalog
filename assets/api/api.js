export default class Api {
    constructor() {
        this._config = {
            credentials: 'include',
            headers: {
                Accept: 'application/vnd.api+json',
                'Content-Type': 'application/vnd.api+json'
            }
        };
    }

    async _fetch({ path, options, params }) {
        const url = this.createUrlWithParams(path, params);
        const response = await fetch(url, {
            ...this._config,
            ...options
        });

        if (response.status === 200 || response.status === 201) {
            return options.blob
                ? await response.blob()
                : await response.json();
        }

        if (response.status === 204) {
            return true;
        }

        const msg = await response.json();

        throw new Error(
            msg.title ? `${msg.title}: ${msg.detail}` : response.statusText
        );
    }

    createUrlWithParams(path, params) {
        let url = new URL(path);

        Object.keys(params).forEach(key => {
            if (typeof params[key] === 'undefined' || params[key] === null) {
                return;
            }

            if (Array.isArray(params[key])) {
                params[key].forEach(item => url.searchParams.append(`${key}[]`, item));
            } else {
                url.searchParams.append(key, params[key]);
            }
        });

        return url;
    }

    get(path, params) {
        return this._fetch({ path, params, options: { method: 'GET' } });
    }
}
